# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
    new_str = ""
    str.chars.each do |lett|
        next if lett != lett.upcase
        new_str += lett
    end
    new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
    mid = (str.length / 2)
    if str.length.even?
        str[mid - 1..mid]
    else
        str[mid]
    end
        
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
    count = 0
    str.chars.each do |lett|
        if VOWELS.include?(lett.downcase)
            count += 1
        end
    end
count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  num <= 1 ? 1 : num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each do |el|
    new_str += el
    new_str += separator unless el == arr.last
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_with_index do |lett, idx|
     if idx.even?
       new_str << lett.downcase
     else
       new_str << lett.upcase
     end
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  final_str = []
  str.split.each do |word|
    if word.length >= 5
      final_str << word.reverse
    else
      final_str << word
    end
  end
  final_str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  final_arr = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      final_arr << "fizzbuzz"
    elsif num % 3 == 0
      final_arr << "fizz"
    elsif num % 5 == 0
      final_arr << "buzz"
    else
      final_arr << num
    end
  end
  final_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  count = 0
  max = arr.length - 1
  while count <= max
      new_arr << arr.pop
    count += 1
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  else
    (2...num).each do |n|
      if num % n == 0
        return false
      end
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  new_arr = []
  (1..num).each do |n|
    if num % n == 0
      new_arr << n
    end
  end
  new_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  factors.select {|el| prime?(el)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each do |num|
    if num.even?
      evens << num
    else
      odds << num
    end
  end
    if evens.length > odds.length
      return odds[0]
    else
      return evens[0]
    end
end
